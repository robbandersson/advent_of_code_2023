# Read data
import os
import sys
sys.setrecursionlimit(60000)

data_dir = os.path.normpath("data")
with open(data_dir + '/23.txt') as f:
    data = f.read().strip().splitlines()

grid = []
for row in data:
    grid.append(list(row))


def replace_symbols(nested_list):
    if isinstance(nested_list, list):
        return [replace_symbols(item) for item in nested_list]
    if isinstance(nested_list, str):
        return nested_list.replace('<', '.').replace('>', '.').replace('^', '.').replace('v', '.')


grid = replace_symbols(grid)


class Vertex:
    def __init__(self, label, position, category):
        self.label = label
        self.position = position
        self.category = category


class Edge:
    def __init__(self, start_vertex, end_vertex, weight):
        self.start_vertex = start_vertex
        self.end_vertex = end_vertex
        self.weight = weight


class Graph:
    def __init__(self):
        self.vertices = set()
        self.edges = set()
        self.vertex_positions = {}
        self.vertex_labels = {}
        self.vertex_edges = {}
        self.edge_weight = {}

    def add_vertex(self, label, position, category):
        new_vertex = Vertex(label, position, category)
        self.vertices.add(new_vertex)
        self.vertex_positions[label] = position
        self.vertex_labels[position] = label

    def add_edge(self, start_vertex, end_vertex, weight=1):
        # if start_vertex not in self.get_vertex_labels() or end_vertex not in self.get_vertex_labels():
        #     raise ValueError("Start or end vertex doesn't exist in graph!")
        new_edge = Edge(start_vertex, end_vertex, weight)
        self.edges.add(new_edge)
        if start_vertex in self.vertex_edges:
            self.vertex_edges[start_vertex].append((start_vertex, end_vertex))
        else:
            self.vertex_edges[start_vertex] = [(start_vertex, end_vertex)]
        self.edge_weight[(start_vertex, end_vertex)] = weight

    def remove_vertex(self, label):
        self.vertices = {vertex for vertex in self.vertices if vertex.label != label}
        #self.edges = {edge for edge in self.edges if label not in (edge.start_vertex, edge.end_vertex)}
        pos = self.vertex_positions[label]
        del self.vertex_positions[label]
        del self.vertex_labels[pos]
        for (start, end) in self.vertex_edges[label]:
            if (end, start) in self.vertex_edges[end]:
                self.vertex_edges[end].remove((end, start))
        del self.vertex_edges[label]

    def get_vertex_label_by_position(self, position):
        for vertex in self.vertices:
            if vertex.position == position:
                return vertex.label
        return None

    def get_vertex_position_by_label(self, label):
        for vertex in self.vertices:
            if vertex.label == label:
                return vertex.position
        return None

    def get_vertex_positions(self):
        return [vertex.position for vertex in self.vertices]

    def get_vertex_labels(self):
        return [vertex.label for vertex in self.vertices]

    def get_vertex(self, label):
        for vertex in self.vertices:
            if vertex.label == label:
                return vertex
        return None

    def get_edge_weight(self, start_vertex, end_vertex):
        for edge in self.edges:
            if edge.start_vertex == start_vertex and edge.end_vertex == end_vertex:
                return edge.weight
        return None

    def get_adjacent_vertices(self, vertex):
        adjacent = set()
        for (start, end) in self.vertex_edges[vertex.label]:
            adjacent.add(end)
        return adjacent

    def contraction(self):
        vertices_to_remove = [vertex for vertex in self.vertices if len(self.get_adjacent_vertices(vertex)) == 2]                        
        for vertex in vertices_to_remove:
            adjacent_vertices = list(self.get_adjacent_vertices(vertex))
            if len(adjacent_vertices) == 2:
                new_weight = 0
                for neighbour in adjacent_vertices:
                    new_weight += self.edge_weight[(vertex.label, neighbour)]
                self.add_edge(adjacent_vertices[0], adjacent_vertices[1], new_weight)
                self.add_edge(adjacent_vertices[1], adjacent_vertices[0], new_weight)
                # Remove old vertex
                self.remove_vertex(vertex.label)

    def longest_path_dfs(self, current, end, visited, current_path, current_length, max_path):
        visited.add(current)
        current_path.append(current)

        if current == end and current_length > max_path['length']:
            max_path['path'] = current_path.copy()
            max_path['length'] = current_length

        for neighbour in self.get_adjacent_vertices(self.get_vertex(current)):
            weight = self.edge_weight[(current, neighbour)]
            if neighbour not in visited and weight:
                self.longest_path_dfs(neighbour, end, visited, current_path, current_length + weight, max_path)

        visited.remove(current)
        current_path.pop()

    def longest_path(self, start, end):
        visited = set()
        max_path = {'path': [], 'length': float('-inf')}
        self.longest_path_dfs(start, end, visited, [], 0, max_path)
        return max_path['path'], max_path['length']


def create_graph(grid):
    rows, cols = len(grid), len(grid[0])
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]

    graph = Graph()

    # Add vertices
    label = 0
    for i in range(rows):
        for j in range(cols):
            if grid[i][j] == '#':
                continue
            category = grid[i][j]
            graph.add_vertex(label, (i, j), category)
            label += 1

    # Add edges
    for lbl in range(label):
        position = graph.vertex_positions[lbl]
        for step in directions:
            (i, j) = (position[0] + step[0], position[1] + step[1])
            if (i, j) in graph.vertex_labels.keys():
                neighbour = graph.vertex_labels[(i, j)]
                graph.add_edge(lbl, neighbour)

    return graph

# Create graph
graph = create_graph(grid)

last_vertex = len(graph.get_vertex_positions()) - 1

# Collapse graph
graph.contraction()

# Find longest path
longest_path, longest_path_length = graph.longest_path(0, last_vertex)
print(longest_path_length)
