import os
import sys
from functools import cache

sys.setrecursionlimit(60000)

data_dir = os.path.normpath("data")
with open(data_dir + '/18.txt') as f:
    data = f.read().strip().splitlines()


# Find limits of grid
current_node = (0, 0)
min_row, min_col, max_row, max_col = (0, 0, 0, 0)
for entry in data:
    (row, col) = current_node
    direction, steps, color = entry.split(' ')
    if direction == 'R':
        (row_next, col_next) = (row, col + int(steps))
    elif direction == 'L':
        (row_next, col_next) = (row, col - int(steps))
    elif direction == 'U':
        (row_next, col_next) = (row - int(steps), col)
    else:
        (row_next, col_next) = (row + int(steps), col)
    min_row = min(min_row, row_next)
    min_col = min(min_col, col_next)
    max_row = max(max_row, row_next)
    max_col = max(max_col, col_next)
    current_node = (row_next, col_next)

rows = max_row - min_row + 1
cols = max_col - min_col + 1
grid = [[0] * cols for _ in range(rows)]

start_row = 0 - min_row
start_col = 0 - min_col
current_node = (start_row, start_col)
boundary_nodes = 0
for entry in data:
    (row, col) = current_node
    direction, steps, color = entry.split(' ')
    for k in range(1, int(steps) + 1):
        if direction == 'R':
            grid[row][col + k] = 1
            current_node = (row, col + k)
        elif direction == 'L':
            grid[row][col - k] = 1
            current_node = (row, col - k)
        elif direction == 'U':
            grid[row - k][col] = 1
            current_node = (row - k, col)
        else:
            grid[row + k][col] = 1
            current_node = (row + k, col)
    boundary_nodes += int(steps)


def print_grid(grid):
    for sublist in grid:
        print(''.join(map(str, sublist)))


def is_valid(x, y, rows, cols):
    return 0 <= x < rows and 0 <= y < cols


def dfs(grid, x, y, visited):
    if not is_valid(x, y, len(grid), len(grid[0])) or grid[x][y] == 1 or visited[x][y]:
        return 0

    visited[x][y] = True
    count = 1
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    for dx, dy in directions:
        count += dfs(grid, x + dx, y + dy, visited)

    return count


def find_enclosed_nodes(grid, interior_point):
    rows, cols = len(grid), len(grid[0])
    visited = [[False] * cols for _ in range(rows)]

    start_x, start_y = interior_point

    enclosed_nodes = dfs(grid, start_x, start_y, visited)

    return enclosed_nodes


row = rows // 2
col = 0
on_edge = None
passed = None
while True:
    if grid[row][col] == 1:
        on_edge = True
        passed = True
    else:
        on_edge = False

    if not on_edge and passed:
        break

    col += 1

interior_point = (row, col)
#enclosed_nodes_count = find_enclosed_nodes(grid, interior_point)
#print(enclosed_nodes_count + boundary_nodes)

# Part 2 - Using Pick's theorem
pos = (0, 0)
coordinates = [pos]
min_y = 0
circumference = 0
for entry in data:
    _, _, hexadecimal = entry.split(' ')
    hexadecimal = hexadecimal.replace('(' or ')', '')
    step = int(hexadecimal[1:6], 16)
    direction = hexadecimal[6]
    if direction == '0':
        new_pos = (pos[0], pos[1] + step)
    elif direction == '1':
        new_pos = (pos[0] - step, pos[1])
    elif direction == '2':
        new_pos = (pos[0], pos[1] - step)
    else:
        new_pos = (pos[0] + step, pos[1])
    coordinates.append(new_pos)
    min_y = min(pos[1], min_y)
    pos = new_pos
    circumference += step

coordinates = [(x, y - min_y) for (x, y) in coordinates]
interior_area = 0
for k in range(len(coordinates) - 1):
    x1, y1 = coordinates[k]
    x2, y2 = coordinates[k + 1]
    interior_area += x1 * y2 - x2 * y1

interior_area = abs(interior_area / 2)
area = interior_area + circumference / 2 + 1
print(area)
