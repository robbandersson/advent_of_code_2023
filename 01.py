import os
import argparse
import re
data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solve today\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def get_num(entry, numbers):
    first, last = None, None
    for i, elem in enumerate(entry):
        if elem.isdigit():
            if not first:
                first = elem
            last = elem
        else:
            for key in numbers.keys():
                if key == entry[i:i+len(key)]:
                    if not first:
                        first = str(numbers[key])
                    last = str(numbers[key])                    
    return int(first + last)


def compute():
    match problem:
        case 1:
            with open(data_dir + '/01_1.txt') as f:
                data = f.read().splitlines()
            tot = 0
            for entry in data:
                digits = re.sub('\D', '', entry)
                tot += int(digits[0] + digits[-1])
            return tot
        case 2:
            with open(data_dir + '/01_2.txt') as f:
                data = f.read().splitlines()
            tot = 0
            numbers = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5,
                       'six': 6, 'seven': 7, 'eight': 8, 'nine': 9}
            for entry in data:
                tot += get_num(entry, numbers)
            return tot
        case _:
            return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())


