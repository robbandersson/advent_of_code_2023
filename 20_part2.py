import os
import math

data_dir = os.path.normpath("data")
with open(data_dir + '/20.txt') as f:
    data = f.read().strip().splitlines()


def get_state(modules):
    state = ''
    for key in sorted(list(modules.keys())):
        if modules[key]['cat'] == 'flip_flop':
            state += str(modules[key]['status'])
        elif key == 'broadcaster':
            pass
        else:
            for inner_key in sorted(list(modules[key]['status'])):
                state += modules[key]['status'][inner_key]
    return state


def cyclic_button_push(modules, last_module):
    states = {}
    iteration = 0
    flag = False
    while True:
        iteration += 1
        queue = [('button', 'broadcaster', 'low')]
        while queue:
            sender, receiver, signal = queue.pop(0)
            if receiver == last_module and signal == 'low':                
                flag = True
                break
            next_sender = receiver
            if receiver not in modules:
                continue
            else:
                if modules[receiver]['cat'] == 'flip_flop':
                    if signal == 'low':
                        if modules[receiver]['status'] == 'off':
                            modules[receiver]['status'] = 'on'
                            next_signal = 'high'
                        else:
                            modules[receiver]['status'] = 'off'
                            next_signal = 'low'
                        for next_receiver in modules[receiver]['destinations']:
                            queue.append((next_sender, next_receiver, next_signal))

                elif modules[receiver]['cat'] == 'conjunction':
                    modules[receiver]['status'][sender] = signal
                    for connected in modules[receiver]['status'].values():
                        if connected == 'low':
                            next_signal = 'high'
                            break
                    else:
                        next_signal = 'low'
                    for next_receiver in modules[receiver]['destinations']:
                        queue.append((next_sender, next_receiver, next_signal))

                elif receiver == 'broadcaster':
                    for next_receiver in modules[receiver]['destinations']:
                        queue.append((next_sender, next_receiver, signal))

        if flag:
            return iteration
        current_state = get_state(modules)
        if current_state in states:
            break
        else:
            states[iteration] = current_state


last_modules = ['bm', 'tn', 'dr', 'cl']
iterations = list()
for last_mod in last_modules:
    modules = {}
    connected_input = {}
    for entry in data:
        parts = entry.split()
        name = ''.join(char for char in parts[0] if char.isalpha())

        if '%' in entry:
            modules[name] = {'cat': 'flip_flop', 'status': 'off'}
        elif '&' in entry:
            modules[name] = {'cat': 'conjunction', 'status': {}}
        else:
            modules[name] = {'cat': 'other'}
        modules[name]['destinations'] = [s.replace(',', '') for s in parts[2:]]
        for dest_module in modules[name]['destinations']:
            if dest_module in connected_input:
                connected_input[dest_module].append(name)
            else:
                connected_input[dest_module] = [name]

    for key, vals in modules.items():
        if vals['cat'] == 'conjunction':
            vals['status'] = {connected: 'low' for connected in connected_input[key]}

    target_modules = set()
    path = [last_mod]
    while path:
        current = path.pop(0)
        target_modules.add(current)
        if current == 'broadcaster':
            continue
        for mod in connected_input[current]:
            if mod not in target_modules:
                path.append(mod)

    alt_modules = {key: modules[key] for key in target_modules}
    iterations.append(cyclic_button_push(alt_modules, last_mod))


def lcm(a, b):
    return abs(a * b) // math.gcd(a, b)


def lcm_of_list(numbers):
    result = 1
    for number in numbers:
        result = lcm(result, number)
    return result


print(lcm_of_list(iterations))
