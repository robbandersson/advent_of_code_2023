import os

data = []
with open(os.path.normpath("data") + '/03.txt') as f:
    for line in f:
        data.append(line.strip())


def adjacent_symb(row, col, l, num_rows, num_cols):
    for i in range(max(row - 1, 0), min(row + 2, num_rows)):
        for j in range(max(col - l, 0), min(col + 2, num_cols)):
            if (not data[i][j].isdigit()) and (not data[i][j] == '.'):
                return True
    return False


tot = 0
num_rows = len(data)
num_cols = len(data[0])
for row in range(num_rows):
    num = None
    found_num = False
    for col in range(num_cols):
        if data[row][col].isdigit():
            if found_num:
                num += data[row][col]
            else:
                found_num = True
                num = data[row][col]
            if col == num_cols - 1:
                if adjacent_symb(row, col, len(num), num_rows, num_cols):
                    tot += int(num)
                num = None
                found_num = False
            elif not data[row][col + 1].isdigit():
                if adjacent_symb(row, col, len(num), num_rows, num_cols):
                    tot += int(num)
                num = None
                found_num = False

print(tot)


def find_adjacent_stars(star_map, num, row, col, num_rows, num_cols):
    for i in range(max(0, row - 1), min(row + 2, num_rows)):
        for j in range(max(col - len(num), 0), min(col + 2, num_cols)):
            if data[i][j] == '*':
                if (i, j) in star_map:
                    star_map[(i, j)].append(int(num))
                else:
                    star_map[(i, j)] = [int(num)]
    return star_map


tot = 0
row = 0
star_map = {}
while row < num_rows:
    col = 0
    while col < num_cols:
        if data[row][col].isdigit():
            num = data[row][col]
            flag = True
            while flag:
                if col + 1 == num_cols:
                    flag = False
                elif data[row][col + 1].isdigit():
                    num += data[row][col + 1]
                    col += 1
                else:
                    flag = False
            star_map = find_adjacent_stars(star_map, num, row, col, num_rows, num_cols)
        col += 1
    row += 1


for key, val in star_map.items():
    if len(val) == 2:
        tot += val[0] * val[1]

print(tot)