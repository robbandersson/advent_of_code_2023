import os

data_dir = os.path.normpath("data")
with open(data_dir + '/16_test.txt') as f:
    data = f.read().strip().splitlines()


def in_cycle(path):
    if len(path) < 2:
        return False

    last_pos = path[-1]
    second_to_last_pos = path[-2]

    for k in range(len(path) - 2):
        if path[k] == second_to_last_pos and path[k + 1] == last_pos:
            return True

    return False


def beam_path(grid, path=[], direction=None):
    if path == []:
        path.append((0, 0))
        direction = 'SOUTH'
        return beam_path(grid, path, direction)

    path = path.copy()
    curr_pos = path[-1]
    if direction == 'EAST':
        if curr_pos[1] == len(grid[0]) - 1:
            return path

        next_pos = (curr_pos[0], curr_pos[1] + 1)
        path.append(next_pos)
        if in_cycle(path):
            return path
        next_type = grid[next_pos[0]][next_pos[1]]
        if next_type == '.' or next_type == '-':
            return beam_path(grid, path, direction)
        if next_type == '/':
            return beam_path(grid, path, 'NORTH')
        if next_type == "\\":
            return beam_path(grid, path, 'SOUTH')
        if next_type == '|':
            return beam_path(grid, path, 'NORTH') + beam_path(grid, path,
                                                              'SOUTH')

    elif direction == 'WEST':
        if curr_pos[1] == 0:
            return path

        next_pos = (curr_pos[0], curr_pos[1] - 1)
        path.append(next_pos)
        if in_cycle(path):
            return path
        next_type = grid[next_pos[0]][next_pos[1]]
        if next_type == '.' or next_type == '-':
            return beam_path(grid, path, direction)
        if next_type == '/':
            return beam_path(grid, path, 'SOUTH')
        if next_type == "\\":
            return beam_path(grid, path, 'NORTH')
        if next_type == '|':
            return beam_path(grid, path, 'NORTH') + beam_path(grid, path,
                                                              'SOUTH')

    elif direction == 'NORTH':
        if curr_pos[0] == 0:
            return path

        next_pos = (curr_pos[0] - 1, curr_pos[1])
        path.append(next_pos)
        if in_cycle(path):
            return path
        next_type = grid[next_pos[0]][next_pos[1]]
        if next_type == '.' or next_type == '|':
            return beam_path(grid, path, direction)
        if next_type == '/':
            return beam_path(grid, path, 'EAST')
        if next_type == "\\":
            return beam_path(grid, path, 'WEST')
        if next_type == '-':
            return beam_path(grid, path, 'EAST') + beam_path(grid, path,
                                                             'WEST')

    else:
        if curr_pos[0] == len(grid) - 1:
            return path

        next_pos = (curr_pos[0] + 1, curr_pos[1])
        path.append(next_pos)
        if in_cycle(path):
            return path
        next_type = grid[next_pos[0]][next_pos[1]]
        if next_type == '.' or next_type == '|':
            return beam_path(grid, path, direction)
        if next_type == '/':
            return beam_path(grid, path, 'WEST')
        if next_type == "\\":
            return beam_path(grid, path, 'EAST')
        if next_type == '-':
            return beam_path(grid, path, 'WEST') + beam_path(grid, path,
                                                             'EAST')


grid = []
for row in data:
    grid.append(list(row))
passed_tiles = set(beam_path(grid))
print(len(passed_tiles))

# tile_grid = []
# for i in range(len(grid)):
#     inner = []
#     for j in range(len(grid[0])):
#         if (i, j) in passed_tiles:
#             inner.append('#')
#         else:
#             inner.append('.')
#     tile_grid.append(inner)


# def print_grid(grid):
#     for sublist in grid:
#         print(''.join(map(str, sublist)))


# print_grid(tile_grid)


