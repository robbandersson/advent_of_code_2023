import os

data_dir = os.path.normpath("data")
with open(data_dir + '/13.txt') as f:
    data = f.read().strip().splitlines()


def reflective_val(pattern):
    scale_factor = 100
    val = 0
    for k in range(2):
        num_rows = len(pattern)
        for current_row in range(1, num_rows):
            row_1 = current_row - 1
            row_2 = current_row
            while row_1 >= 0 and row_2 < num_rows:
                if pattern[row_1] == pattern[row_2]:
                    row_1 -= 1
                    row_2 += 1
                else:
                    break
            else:
                val = scale_factor * current_row
                break
        else:
            pattern = [''.join(row) for row in zip(*pattern)]
            scale_factor /= 100
            continue
        break
    return val


pattern = []
tot = 0
for row in data:
    if row == '':
        tot += reflective_val(pattern)
        pattern = []
    else:
        pattern.append(row)
tot += reflective_val(pattern)


print(tot)

# Part 2


def reflective_val_list(pattern):
    scale_factor = 100
    val = []
    for k in range(2):
        num_rows = len(pattern)
        for current_row in range(1, num_rows):
            row_1 = current_row - 1
            row_2 = current_row
            while row_1 >= 0 and row_2 < num_rows:
                if pattern[row_1] == pattern[row_2]:
                    row_1 -= 1
                    row_2 += 1
                else:
                    break
            else:
                val.append(scale_factor * current_row)

        pattern = [''.join(row) for row in zip(*pattern)]
        scale_factor /= 100

    return val


def replace_pattern(pattern, idx):
    row = idx // len(pattern[0])
    col = idx % len(pattern[0])
    s = list(pattern[row])
    if pattern[row][col] == '#':
        s[col] = '.'
    else:
        s[col] = '#'
    pattern[row] = "".join(s)
    return pattern


def smudged_reflective_val(pattern):
    idx = 0
    reference = reflective_val(pattern)
    while True:
        alt_pattern = replace_pattern(pattern.copy(), idx)
        val = reflective_val_list(alt_pattern)
        if len(val) > 0:
            if reference in val:
                val.remove(reference)
            if len(val) > 0:
                val = val[0]
                break
        idx += 1
    return val


pattern = []
tot = 0
i = 0
for row in data:
    if row == '':
        i += 1
        tot += smudged_reflective_val(pattern)
        pattern = []
    else:
        pattern.append(row)
tot += smudged_reflective_val(pattern)

print(tot)
