import os
data_dir = os.path.normpath("data")
with open(data_dir + '/07.txt') as f:
    data = f.read().splitlines()


def val_of_hand(hand):
    card_val = {}
    for i in range(2, 10):
        card_val[str(i)] = i
    card_val['T'] = 10
    card_val['J'] = 11
    card_val['Q'] = 12
    card_val['K'] = 13
    card_val['A'] = 14

    if all(card == hand[0] for card in hand):
        return [7, card_val[hand[0]]]
    else:
        card_count = {}
        for card in hand:
            card_count[card] = card_count.get(card, 0) + 1
        # Full house or four of a kind
        if len(card_count) == 2:
            one = None
            two = None
            three = None
            four = None
            for card, count in card_count.items():
                if count == 4:
                    four = card
                elif count == 3:
                    three = card
                elif count == 2:
                    two = card
                else:
                    one = card
            if four:
                return [6, card_val[four], card_val[one]]
            return [5, card_val[three], card_val[two]]
        # Three of a kind or two pairs
        elif len(card_count) == 3:
            two = None
            one = None
            three = None
            for card, count in card_count.items():
                if count == 3:
                    three = [card_val[card]]
                elif count == 2:
                    if not two:
                        two = [card_val[card]]
                    else:
                        two.append(card_val[card])
                        two = sorted(two, reverse=True)                        
                else:
                    if not one:
                        one = [card_val[card]]
                    else:
                        one.append(card_val[card])
                        one = sorted(one, reverse=True)
            if three:
                return [4] + three + one
            return [3] + two + one
        # Pair
        elif len(card_count) == 4:
            one = None
            for card, count in card_count.items():
                if count == 2:
                    pair = [card_val[card]]
                else:
                    if not one:
                        one = [card_val[card]]
                    else:
                        one.append(card_val[card])
                        one = sorted(one, reverse=True)
            return [2] + pair + one
        else:
            cards = []
            for card in card_count.keys():
                cards.append(card_val[card])
            return [1] + sorted(cards, reverse=True)


order = []
for k, line in enumerate(data):
    hand, num = line.split()
    if order:
        for pos, [comp_hand, n, comp_hand_val] in enumerate(order):
            compare = True
            insert_flag = False
            i = 0
            hand_val = val_of_hand(hand)
            while compare:
                if comp_hand_val[i] == hand_val[i]:
                    i += 1
                elif comp_hand_val[i] > hand_val[i]:
                    compare = False
                else:
                    compare = False
                    insert_flag = True
                    order.insert(pos, [hand, int(num), val_of_hand(hand)])
            if insert_flag:
                break
        if not insert_flag:
            order.insert(pos + 1, [hand, int(num), val_of_hand(hand)])
    else:
        order.append([hand, int(num), val_of_hand(hand)])

#     if k > 100:
#         break
# for val in order:
#     print(val)


tot = 0
num_hands = len(order)
for k in range(num_hands):
    print(1000 - k, (num_hands - k) * order[k][1], order[k][0])
    tot += (num_hands - k) * order[k][1]
print(tot)