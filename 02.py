import re
import os
with open(os.path.normpath("data") + '/02.txt') as f:
    data = f.read().splitlines()

tot = 0
for row in data:
    flag = True
    for val in re.split(r'[,:;]', row):
        num = int(re.findall('\d+', val)[0])
        if "green" in val:
            if num > 13:
                flag = False
                break
        elif "red" in val:
            if num > 12:
                flag = False
                break
        elif "blue" in val:
            if num > 14:
                flag = False
                break
        else:
            game = num
    if flag:
        tot += game

print(tot)

tot = 0
for row in data:
    green = 0
    red = 0
    blue = 0
    for val in re.split(r'[,:;]', row):
        num = int(re.findall('\d+', val)[0])
        if "green" in val:
            if num > green:
                green = num
        elif "red" in val:
            if num > red:
                red = num
        elif "blue" in val:
            if num > blue:
                blue = num
    tot += green * red * blue

print(tot)
