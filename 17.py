import os
import heapq

data_dir = os.path.normpath("data")
with open(data_dir + '/17.txt') as f:
    data = f.read().strip().splitlines()


def intermediate_weight(grid, node1, node2):
    x1, y1 = node1
    x2, y2 = node2

    if abs(y1 - y2) + abs(x1 - x2) >= 2:
        if x1 == x2:
            return sum(grid[x1][y] for y in range(min(y1, y2) + 1, max(y1, y2)))
        return sum(grid[x][y1] for x in range(min(x1, x2) + 1, max(x1, x2)))
    else:
        return 0


def change_direction(current_step, new_step):
    if current_step == (0, 0):
        return True

    row1, col1 = current_step
    row2, col2 = new_step
    if (row1 - row2 != 0) and (col1 - col2 != 0):
        return True
    return False


def opposite_direction(current_step, new_step):
    row1, col1 = current_step
    row2, col2 = new_step
    if row1 == row2 and col1 * col2 < 0:
        return True
    if col1 == col2 and row1 * row2 < 0:
        return True
    return False


def dijkstra_constrained(grid, start_node, start_step):
    rows, cols = len(grid), len(grid[0])

    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    moves = []
    for direction in directions:
        for step in range(1, 4):
            moves.append(tuple(map(lambda x: x * step, direction)))

    distances = {(r, c): {(sr, sc): float('infinity') for (sr, sc) in moves if
                 0 <= r - sr < rows and 0 <= c - sc < cols}
                 for r in range(rows) for c in range(cols)}
    distances[start_node][start_step] = 0
    priority_queue = [(0, start_node, start_step)]

    while priority_queue:
        current_distance, current_node, current_step = heapq.heappop(priority_queue)

        if current_distance > distances[current_node][current_step]:
            continue

        current_row, current_col = current_node
        current_row_step, current_col_step = current_step

        for new_step in moves:
            if not opposite_direction(current_step, new_step):
                if change_direction(current_step, new_step):
                    new_row_step, new_col_step = new_step
                else:
                    new_row_step = current_row_step + new_step[0]
                    new_col_step = current_col_step + new_step[1]

                if abs(new_row_step) <= 3 and abs(new_col_step) <= 3:
                    new_row = current_row + new_step[0]
                    new_col = current_col + new_step[1]

                    if 0 <= new_row < rows and 0 <= new_col < cols:
                        weight = grid[new_row][new_col] + intermediate_weight(grid, current_node, (new_row, new_col))
                        distance = current_distance + weight

                        if distance < distances[(new_row, new_col)][(new_row_step, new_col_step)]:
                            distances[(new_row, new_col)][(new_row_step, new_col_step)] = distance
                            heapq.heappush(priority_queue, (distance, (new_row, new_col), (new_row_step, new_col_step)))

    return min(distances[(rows - 1, cols - 1)].values())


grid = []
for row in data:
    grid.append(list(map(int, row)))


start_node = (0, 0)
start_step = (0, 0)
distances = dijkstra_constrained(grid, start_node, start_step)
print(distances)

# Part 2

def dijkstra_constrained_2(grid, start_node, start_step):
    rows, cols = len(grid), len(grid[0])

    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    moves = []
    for direction in directions:
        for step in range(4, 11):
            moves.append(tuple(map(lambda x: x * step, direction)))

    distances = {(r, c): {(sr, sc): float('infinity') for (sr, sc) in moves if
                 0 <= r - sr < rows and 0 <= c - sc < cols}
                 for r in range(rows) for c in range(cols)}
    distances[start_node][start_step] = 0
    priority_queue = [(0, start_node, start_step)]

    while priority_queue:
        current_distance, current_node, current_step = heapq.heappop(priority_queue)

        if current_distance > distances[current_node][current_step]:
            continue

        current_row, current_col = current_node
        current_row_step, current_col_step = current_step

        for new_step in moves:
            if not opposite_direction(current_step, new_step):
                if change_direction(current_step, new_step):
                    new_row_step, new_col_step = new_step
                else:
                    new_row_step = current_row_step + new_step[0]
                    new_col_step = current_col_step + new_step[1]

                if abs(new_row_step) <= 10 and abs(new_col_step) <= 10:
                    new_row = current_row + new_step[0]
                    new_col = current_col + new_step[1]

                    if 0 <= new_row < rows and 0 <= new_col < cols:
                        weight = grid[new_row][new_col] + intermediate_weight(grid, current_node, (new_row, new_col))
                        distance = current_distance + weight

                        if distance < distances[(new_row, new_col)][(new_row_step, new_col_step)]:
                            distances[(new_row, new_col)][(new_row_step, new_col_step)] = distance
                            heapq.heappush(priority_queue, (distance, (new_row, new_col), (new_row_step, new_col_step)))

    return min(distances[(rows - 1, cols - 1)].values())


distances = dijkstra_constrained_2(grid, start_node, start_step)
print(distances)