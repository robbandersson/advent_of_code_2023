import os
import math
data_dir = os.path.normpath("data")
with open(data_dir + '/11.txt') as f:
    galaxy = f.read().strip().splitlines()

for k in range(2):
    row = 0
    while row < len(galaxy):
        if all(char == galaxy[row][0] for char in galaxy[row]):
            galaxy.insert(row + 1, "."*len(galaxy[0]))
            row += 2
        else:
            row += 1
    galaxy = [''.join(row) for row in zip(*galaxy)]

positions = []
for r, row in enumerate(galaxy):
    for c, col in enumerate(row):
        if galaxy[r][c] == "#":
            positions.append((r, c))

tot = 0
for k in range(len(positions) - 1):
    for j in range(k + 1, len(positions)):
        tot += abs(positions[k][0] - positions[j][0]) + abs(positions[k][1] - positions[j][1])

print(tot)
#print("\n".join(galaxy))

## Part 2
with open(data_dir + '/11.txt') as f:
    galaxy = f.read().strip().splitlines()

positions = []
for r, row in enumerate(galaxy):
    for c, col in enumerate(row):
        if galaxy[r][c] == "#":
            positions.append((r, c))

distance = {}
for val in positions:
    distance[val] = list(val)

magnitude = 999999

for k in range(2):
    row = 0
    while row < len(galaxy):
        if all(char == galaxy[row][0] for char in galaxy[row]):
            for key, val in distance.items():
                if k == 0 and key[0] > row:
                    val[0] += magnitude
                elif k == 1 and key[1] > row:
                    val[1] += magnitude
        row += 1
    galaxy = [''.join(row) for row in zip(*galaxy)]

#print("\n".join(galaxy))
# for key, val in distance.items():
#     print(key, val)

def manhattan(a1, b1, a2, b2):
    return abs(a1 - a2) + abs(b1 - b2)


tot = 0
for k in range(len(positions) - 1):
    for j in range(k + 1, len(positions)):
        tot += manhattan(*distance[positions[k]], *distance[positions[j]])
        
print(tot)
#print(positions)
