import os
import re
import math
data_dir = os.path.normpath("data")
with open(data_dir + '/08.txt') as f:
    data = f.read().splitlines()

instructions = data.pop(0)
data.pop(0)

network = {}
for line in data:
    line = line.split()
    key = re.findall(r'[a-zA-Z0-9]+', line[0])[0]
    left = re.findall(r'[a-zA-Z0-9]+', line[2])[0]
    right = re.findall(r'[a-zA-Z0-9]+', line[3])[0]
    network[key] = {"left": left,
                    "right": right}

loc = 'AAA'
idx = 0
num_steps = 0
while loc != 'ZZZ':
    if instructions[idx] == 'L':
        path = "left"
    else:
        path = "right"
    loc = network[loc][path]
    idx += 1
    if idx > len(instructions) - 1:
        idx = 0
    num_steps += 1

#print(num_steps)

### Part 2
start_nodes = [key for key in network if key.endswith("A")]
cycles = []
for current in start_nodes:
    cycle = []
    first_z = None
    step_count = 0
    while True:
        while step_count == 0 or not current.endswith("Z"):
            step_count += 1
            current = network[current]["left" if instructions[0] == "L" else "right"]
            instructions = instructions[1:] + instructions[0]

        cycle.append(step_count)
        if first_z is None:
            first_z = current
            step_count = 0
        elif current == first_z:
            break
    cycles.append(cycle)

nums = [cycle[0] for cycle in cycles]
lcm = nums.pop()
for num in nums:
    lcm = lcm * num // math.gcd(lcm, num)

print(lcm)