import os
import numpy as np
from itertools import combinations
from scipy.optimize import fsolve, root


data_dir = os.path.normpath("data")
with open(data_dir + '/24.txt') as f:
    data = f.read().strip().splitlines()

#low_x, high_x, low_y, high_y = (7, 27, 7, 27)
low_x, high_x, low_y, high_y = (200000000000000, 400000000000000, 200000000000000, 400000000000000)

intersections = []
for row in data:
    points = []
    coord, velocity = row.split('@')
    (x, y) = tuple(list(map(int, coord.split(',')))[0:2])
    (vx, vy) = tuple(list(map(int, velocity.split(',')))[0:2])
    if low_x <= x <= high_x and low_y <= y <= high_y:
        points.append((x, y))
    if (k := (low_x - x) / vx) > 0:
        if low_y <= y + k * vy <= high_y:
            points.append((x + k * vx, y + k * vy))
    if (k := (high_x - x) / vx) > 0:
        if low_y <= y + k * vy <= high_y:
            points.append((x + k * vx, y + k * vy))
    if (k := (low_y - y) / vy) > 0:
        if low_x <= x + k * vx <= high_x:
            points.append((x + k * vx, y + k * vy))
    if (k := (high_y - y) / vy) > 0:
        if low_x <= x + k * vx <= high_x:
            points.append((x + k * vx, y + k * vy))
    if len(points) == 2:
        intersections.append(points)


def orientation(P1, P2, P3):
    return (P2[1] - P1[1]) * (P3[0] - P2[0]) - (P2[0] - P1[0]) * (P3[1] - P2[1])


tot = 0
comb = combinations(intersections, 2)
for points in comb:
    P1, Q1 = points[0]
    P2, Q2 = points[1]
    if orientation(P1, Q1, P2) * orientation(P1, Q1, Q2) < 0 and orientation(P2, Q2, P1) * orientation(P2, Q2, Q1) < 0:
        tot += 1

print(tot)

# Part 2

from sympy import symbols, Eq, solve


initial_positions, velocities = [], []
for i, row in enumerate(data):
    if i >= 3:
        break
    coord, velocity = row.split('@')
    (x, y, z) = tuple(list(map(int, coord.split(','))))
    (vx, vy, vz) = tuple(list(map(int, velocity.split(','))))
    initial_positions.append((x, y, z))
    velocities.append((vx, vy, vz))


x, y, z, vx, vy, vz = symbols('x y z vx vy vz')
k0, k1, k2 = symbols('k0 k1 k2', positive=True)


equations = [
    Eq((x - initial_positions[0][0]) + k0 * (vx - velocities[0][0]), 0),
    Eq((y - initial_positions[0][1]) + k0 * (vy - velocities[0][1]), 0),
    Eq((z - initial_positions[0][2]) + k0 * (vz - velocities[0][2]), 0),
    Eq((x - initial_positions[1][0]) + k1 * (vx - velocities[1][0]), 0),
    Eq((y - initial_positions[1][1]) + k1 * (vy - velocities[1][1]), 0),
    Eq((z - initial_positions[1][2]) + k1 * (vz - velocities[1][2]), 0),
    Eq((x - initial_positions[2][0]) + k2 * (vx - velocities[2][0]), 0),
    Eq((y - initial_positions[2][1]) + k2 * (vy - velocities[2][1]), 0),
    Eq((z - initial_positions[2][2]) + k2 * (vz - velocities[2][2]), 0),
]

sol = solve(equations, x, y, z, vx, vy, vz, k0, k1, k2)
print(sol[0][0] + sol[0][1] + sol[0][2])
