#
# Solution to Advent of Code 2023 Day 5 - If You Give A Seed A Fertilizer
#
# https://gitlab.com/robbandersson/advent_of_code_2023
#
import re
import os
import numpy as np
data_dir = os.path.normpath("data")


def compute():
    with open(data_dir + '/05.txt') as f:
        data = f.read().splitlines()
    part = 2

    seeds = []
    maps = {}
    key = 0
    for line in data:
        if "seeds" in line:
            nums = re.findall(r"\d+", line)
            seeds = [int(num) for num in nums]
        elif line != '':
            if "map" in line:
                key += 1
            else:
                nums = re.findall(r"\d+", line)
                mappings = [int(num) for num in nums]
                if key in maps:
                    maps[key].append(mappings)
                else:
                    maps[key] = [mappings]
    num_keys = key

    lowest_location = None
    if part == 1:
        for seed in seeds:
            location = seed
            for key in range(1, num_keys + 1):
                for val in maps[key]:
                    source = val[1]
                    dest = val[0]
                    steps = val[2]
                    if location >= source and location < source + steps:
                        location = dest + (location - source)
                        break
            if not lowest_location:
                lowest_location = location
            elif location < lowest_location:
                lowest_location = location
    else:
        seeds = [(seeds[2 * k], seeds[2 * k] + seeds[2 * k + 1]) for k in range(len(seeds) // 2)]
        layer = 1
        while layer <= num_keys:            
            new_seeds = []
            while seeds:
                s, e = seeds.pop(0)
                for a, b, c in maps[layer]:
                    os = max(s, b)
                    oe = min(e, b + c)
                    if os < oe:
                        new_seeds.append((os - b + a, oe - b + a))
                        if os > s:
                            seeds.append((s, os))
                        if e > oe:
                            seeds.append((oe, e))
                        break
                else:
                    new_seeds.append((s, e))
            seeds = new_seeds
            layer += 1
    return min(seeds)


if __name__ == '__main__':
    print(compute())

