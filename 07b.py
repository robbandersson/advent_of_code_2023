import os
data_dir = os.path.normpath("data")
with open(data_dir + '/07.txt') as f:
    data = f.read().splitlines()


letter_map = {"T": "A", "J": ".", "Q": "C", "K": "D", "A": "E"}


def score(hand):
    counts = [hand.count(card) for card in hand]
    if 5 in counts:
        return 6
    if 4 in counts:
        return 5
    if 3 in counts:
        if 2 in counts:
            return 4
        else:
            return 3
    if counts.count(2) == 4:
        return 2
    if 2 in counts:
        return 1
    else:
        return 0


def replacements(hand):
    if hand == "":
        return [""]

    return [
        x + y
        for x in ("23456789TQKA" if hand[0] == "J" else hand[0])
        for y in replacements(hand[1:])
        ]

def classify(hand):
    return max(map(score, replacements(hand)))


def strength(hand):
    return (classify(hand), [letter_map.get(card, card) for card in hand])


plays = []
for line in data:
    hand, bid = line.split()
    plays.append((hand, int(bid)))

plays.sort(key=lambda play: strength(play[0]))

tot = 0
for rank, (hand, bid) in enumerate(plays, 1):
    tot += rank * bid
print(tot)