import os
import sys
from functools import cache

sys.setrecursionlimit(5500)

data_dir = os.path.normpath("data")
with open(data_dir + '/16.txt') as f:
    data = f.read().strip().splitlines()


def print_grid(grid):
    for sublist in grid:
        print(''.join(map(str, sublist)))


def get_encounter(grid, pos):
    return grid[pos[0]][pos[1]]


def get_direction(encounter, current_direction):
    if current_direction == 'south':
        if encounter in ('.', '|'):
            return 'south'
        if encounter == '-':
            return ('west', 'east')
        if encounter == '\\':
            return 'east'
        return 'west'

    if current_direction == 'north':
        if encounter in ('.', '|'):
            return 'north'
        if encounter == '-':
            return ('west', 'east')
        if encounter == '\\':
            return 'west'
        return 'east'

    if current_direction == 'west':
        if encounter in ('.', '-'):
            return 'west'
        if encounter == '|':
            return ('north', 'south')
        if encounter == '\\':
            return 'north'
        return 'south'

    if current_direction == 'east':
        if encounter in ('.', '-'):
            return 'east'
        if encounter == '|':
            return ('north', 'south')
        if encounter == '\\':
            return 'south'
        return 'north'


def traverse(grid, path=None, visited=None, direction=None):
    if path is None:
        current = (0, 0)
        path = [current]
        encounter = get_encounter(grid, current)
        direction = 'east'
        visited = {(tuple(current), direction)}
        next_direction = get_direction(encounter, direction)
        return traverse(grid, path, visited, next_direction)

    current = path[-1]
    if direction == 'south':
        if current[0] == len(grid) - 1:
            return path

        ensuing = (current[0] + 1, current[1])
        path.append(ensuing)
        encounter = get_encounter(grid, ensuing)
        next_direction = get_direction(encounter, direction)
        if isinstance(next_direction, tuple):
            alt_path = path.copy()
            visited.add((tuple(ensuing), 'east'))
            visited.add((tuple(ensuing), 'west'))
            return traverse(grid, path, visited, 'east') + traverse(grid, alt_path, visited, 'west')
        elif (ensuing, next_direction) in visited:
            return path
        else:
            visited.add((tuple(ensuing), next_direction))
            return traverse(grid, path, visited, next_direction)

    if direction == 'north':
        if current[0] == 0:
            return path

        ensuing = (current[0] - 1, current[1])
        path.append(ensuing)
        encounter = get_encounter(grid, ensuing)
        next_direction = get_direction(encounter, direction)
        if isinstance(next_direction, tuple):
            alt_path = path.copy()
            visited.add((tuple(ensuing), 'east'))
            visited.add((tuple(ensuing), 'west'))
            return traverse(grid, path, visited, 'east') + traverse(grid, alt_path, visited, 'west')
        elif (ensuing, next_direction) in visited:
            return path
        else:
            visited.add((tuple(ensuing), next_direction))
            return traverse(grid, path, visited, next_direction)

    if direction == 'east':
        if current[1] == len(grid[0]) - 1:
            return path

        ensuing = (current[0], current[1] + 1)
        path.append(ensuing)
        encounter = get_encounter(grid, ensuing)
        next_direction = get_direction(encounter, direction)
        if isinstance(next_direction, tuple):
            alt_path = path.copy()
            visited.add((tuple(ensuing), 'south'))
            visited.add((tuple(ensuing), 'north'))
            return traverse(grid, path, visited, 'south') + traverse(grid, alt_path, visited, 'north')
        elif (ensuing, next_direction) in visited:
            return path
        else:
            visited.add((tuple(ensuing), next_direction))
            return traverse(grid, path, visited, next_direction)

    if direction == 'west':
        if current[1] == 0:
            return path

        ensuing = (current[0], current[1] - 1)
        path.append(ensuing)
        encounter = get_encounter(grid, ensuing)
        next_direction = get_direction(encounter, direction)
        if isinstance(next_direction, tuple):
            alt_path = path.copy()
            visited.add((tuple(ensuing), 'south'))
            visited.add((tuple(ensuing), 'north'))
            return traverse(grid, path, visited, 'south') + traverse(grid, alt_path, visited, 'north')
        elif (ensuing, next_direction) in visited:
            return path
        else:
            visited.add((tuple(ensuing), next_direction))
            return traverse(grid, path, visited, next_direction)


grid = []
for row in data:
    grid.append(list(row))

print(len(set(traverse(grid))))


# Part 2

max_val = 0
outer_rows = (0, len(grid) - 1)
outer_cols = (0, len(grid[0]) - 1)
for col in outer_cols:
    for row in range(len(grid)):        
        current = (row, col)
        path = [current]
        directions = []
        if col == 0:
            if row == 0:
                directions.append('east')
                directions.append('south')
            elif row == len(grid) - 1:
                directions.append('east')
                directions.append('north')
            else:
                directions.append('east')
        else:
            if row == 0:
                directions.append('west')
                directions.append('south')
            elif row == len(grid) - 1:
                directions.append('west')
                directions.append('north')
            else:
                directions.append('west')        
        for direc in directions:
            visited = {(tuple(current), direc)}
            encounter = get_encounter(grid, current)
            next_direction = get_direction(encounter, direc)
            if isinstance(next_direction, tuple):
                temp = next_direction
                for direc in temp:
                    val = len(set(traverse(grid, path, visited, direc)))
                    if val > max_val:
                        max_val = max(val, max_val)
                        passed_tiles = set(traverse(grid, path, visited, direc))
                        print(max_val)
            else:
                val = len(set(traverse(grid, path, visited, next_direction)))
                if val > max_val:
                    max_val = max(val, max_val)
                    passed_tiles = set(traverse(grid, path, visited, next_direction))
                    print(max_val)

for row in outer_rows:
    for col in range(1, len(grid[0]) - 1):        
        current = (row, col)
        path = [current]
        if row == 0:
            direction = 'south'
        else:
            direction = 'north'
        visited = {(tuple(current), direction)}
        encounter = get_encounter(grid, current)                    
        next_direction = get_direction(encounter, direc)
        if isinstance(next_direction, tuple):
            temp = next_direction
            for direc in temp:
                val = len(set(traverse(grid, path, visited, direc)))
                if val > max_val:
                    max_val = max(val, max_val)
                    passed_tiles = set(traverse(grid, path, visited, direc))
                    print(max_val)
        else:
            val = len(set(traverse(grid, path, visited, next_direction)))
            if val > max_val:
                max_val = max(val, max_val)
                passed_tiles = set(traverse(grid, path, visited, next_direction))
                print(max_val)
print(max_val)

