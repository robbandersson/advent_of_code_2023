import os
from collections import deque

data_dir = os.path.normpath("data")
with open(data_dir + '/21.txt') as f:
    data = f.read().strip().splitlines()

grid = []
for row in data:
    grid.append(row)


for row_idx, row in enumerate(grid):
    for col_idx, entry in enumerate(row):
        if entry == 'S':
            break
    else:
        continue
    break


def traverse_grid(grid, start, n):
    rows = len(grid)
    cols = len(grid[0])

    directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    visited = {}
    positions = {}

    queue = deque([(start, 0)])

    while queue:
        (x, y), steps = queue.popleft()

        if steps > n:
            break

        if steps in positions:
            positions[steps].add((x, y))
        else:
            positions[steps] = {(x, y)}

        if steps + 1 not in visited:
            visited[steps + 1] = set()

        for dx, dy in directions:
            nx, ny = x + dx, y + dy
            if 0 <= nx < rows and 0 <= ny < cols and (nx, ny) not in visited[steps + 1] and grid[nx][ny] != '#':
                visited[steps + 1].add((nx, ny))
                queue.append(((nx, ny), steps + 1))

    return positions[n]


#positions = traverse_grid(grid, (row_idx, col_idx), 64)
#print(len(positions))


# Part 2
s_pos = (row_idx + 2 * len(grid), col_idx + 2 * len(grid[0]))


grid = []
for k in range(5):
    for row in data:
        grid.append(row + row + row + row + row)

for row_idx, row in enumerate(grid):
    for col_idx, entry in enumerate(row):
        if entry == 'S' and (row_idx, col_idx) != s_pos:
            replace_row = grid[row_idx].replace('S', '.')
            grid[row_idx] = replace_row


row_idx, col_idx = s_pos

reach_1 = len(traverse_grid(grid, (row_idx, col_idx), 65))
reach_2 = len(traverse_grid(grid, (row_idx, col_idx), 65 + 131))
reach_3 = len(traverse_grid(grid, (row_idx, col_idx), 65 + 2 * 131))

a = (reach_3 - 2 * reach_2 + reach_1) // 2
b = (4 * reach_2 - 3 * reach_1 - reach_3) // 2
c = reach_1

print(a * 202300 ** 2 + b * 202300 + c)