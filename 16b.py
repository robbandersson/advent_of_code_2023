import os

data_dir = os.path.normpath("data")
with open(data_dir + '/16_test.txt') as f:
    data = f.read().strip().splitlines()




# Idea: we call recursively the function with a grid and a path
    # from the path, we find the direction (i.e., how we got to 
    # where we are and thus an idea of where to go next. Should 
    # we branch we will copy the path and send the copy to one of the 
    # branches. If we're at a dead end we'll return the set of the path
    # We also ensure that we haven't been at the current position before
    # by taking a similar step with a visited list)        

def find_direction(path):
    if len(path) == 1:
        return 'R'

    pos_1 = path[-1]
    pos_2 = path[-2]
    if pos_1[0] > pos_2[0]:
        return 'U'
    if pos_1[0] < pos_2[0]:
        return 'D'
    if pos_1[1] > pos_2[1]:
        return 'R'

    return 'L'


def traverse(grid, path=None, visited=None):
    if path is None:
        path = []
        path.append((0, 0))
        visited = []
        visited.append(((0, 0), 'R'))
        return traverse(grid, path, visited)

    direction = find_direction(path)
    curr_pos = path[-1]
    encounter = grid[curr_pos[0]][curr_pos[1]]

    if direction == 'R':
        if encounter in ('.', '-'):
            if curr_pos[1] < len(grid[0]) - 1:
                next_pos = (curr_pos[0], curr_pos[1] + 1)
                path.append(next_pos)
                visited.append((next_pos, 'R'))
                return traverse(grid, path, visited)
            else:
                return path
        if encounter == '\\':
            if curr_pos[0] < len(grid) - 1:
                next_pos = (curr_pos[0] + 1, curr_pos[1])
                path.append(next_pos)
                visited.append((next_pos, 'D'))
                return traverse(grid, path, visited)
            else:
                return path
        if encounter == '/':
            if curr_pos[0] > 0:
                next_pos = (curr_pos[0] - 1, curr_pos[1])
                path.append(next_pos)
                visited.append((next_pos, 'U'))
                return traverse(grid, path, visited)
        if encounter == '|':
            if curr_pos[0] == 0:
                next_pos = (curr_pos[0] + 1, curr_pos[1])
                path.append(next_pos)
                visited.append((next_pos, 'D'))
                return traverse(grid, path, visited)
            if curr_pos[0] == len(grid):
                next_pos = (curr_pos[0] - 1, curr_pos[1])
                path.append(next_pos)
                visited.append((next_pos, 'U'))
                return traverse(grid, path, visited)

            next_pos = (curr_pos[0] + 1, curr_pos[0])
            path.append(next_pos)
            visited.append((next_pos, 'D'))

            next_pos = (curr_pos[0] - 1, curr_pos[1])
            new_path = path.copy()
            new_path.append(next_pos)
            visited.append((next_pos, 'U'))
            return traverse(grid, path, visited) + traverse(grid, new_path,
                                                            visited)




grid = []
for row in data:
    grid.append(list(row))

traverse(grid)