import os
import math
data_dir = os.path.normpath("data")
maze = []
with open(data_dir + '/10_test_3.txt') as f:
    maze = f.read().splitlines()

pipe_map = {'|': ('N', 'S'), '-': ('E', 'W'), 'L': ('N', 'E'),
            'J': ('N', 'W'), '7': ('S', 'W'), 'F': ('S', 'E'),
            'S': ('N', 'W', 'S', 'E'), '.': {}}
dist = {}
start_pos = [(row_index, col_index) for row_index, row in enumerate(maze)
             for col_index, char in enumerate(row) if char == 'S'][0]
dist[start_pos] = 0
num_rows = len(maze)
num_cols = len(maze[0])


def is_connected(con_1, con_2):
    if con_1 == 'N':
        if 'S' in con_2:
            return True
    elif con_1 == 'S':
        if 'N' in con_2:
            return True
    elif con_1 == 'W':
        if 'E' in con_2:
            return True
    elif con_1 == 'E':
        if 'W' in con_2:
            return True
    return False


current = [start_pos]
while current:
    pipe_pos = current.pop(0)
    pipe = maze[pipe_pos[0]][pipe_pos[1]]
    next_dist = dist[pipe_pos] + 1

    for connected in pipe_map[pipe]:
        if connected == 'N' and pipe_pos[0] - 1 >= 0:
            new_pos = (pipe_pos[0] - 1, pipe_pos[1])
        elif connected == 'S' and pipe_pos[0] + 1 < num_rows:
            new_pos = (pipe_pos[0] + 1, pipe_pos[1])
        elif connected == 'E' and pipe_pos[1] + 1 < num_cols:
            new_pos = (pipe_pos[0], pipe_pos[1] + 1)
        elif connected == 'W' and pipe_pos[1] - 1 >= 0:
            new_pos = (pipe_pos[0], pipe_pos[1] - 1)

        if maze[new_pos[0]][new_pos[1]] != '.':
            next_pipe = maze[new_pos[0]][new_pos[1]]
            if new_pos not in dist and is_connected(connected, pipe_map[next_pipe]):
                dist[new_pos] = next_dist
                current.append(new_pos)
            elif new_pos in dist and dist[new_pos] > next_dist and is_connected(pipe_map[next_pipe]):
                dist[new_pos] = next_dist
                current.append(new_pos)

print(max(dist.values()))

## Part two.
# Idea - walk from start until back at start. Keep set of all passed to the left and to the right. 
current_pos = start_pos
for connected in pipe_map['S']:
    if connected == 'N' and 'S' in pipe_map[maze[current_pos[0] - 1][current_pos[1]]]:
        direction = 'N'
        break
    elif connected == 'S' and 'N' in pipe_map[maze[current_pos[0] + 1][current_pos[1]]]:
        direction = 'S'
        break
    elif connected == 'W' and 'E' in pipe_map[maze[current_pos[0]][current_pos[1] - 1]]:
        direction = 'W'
        break
    elif connected == 'E' and 'W' in pipe_map[maze[current_pos[0]][current_pos[1] + 1]]:
        direction = 'E'
        break
direction = 'W'

def find_next_pos(current_pos, direction):
    if direction == 'N':
        return (current_pos[0] - 1, current_pos[1])
    elif direction == 'S':
        return (current_pos[0] + 1, current_pos[1])
    elif direction == 'W':
        return (current_pos[0], current_pos[1] - 1)
    elif direction == 'E':
        return (current_pos[0], current_pos[1] + 1)


def find_next_direction(direction, a, b):
    if direction == 'N':
        if a == 'S':
            return b
        else:
            return a
    elif direction == 'S':
        if a == 'N':
            return b
        else:
            return a
    elif direction == 'W':
        if a == 'E':
            return b
        else:
            return a
    elif direction == 'E':
        if a == 'W':
            return b
        else:
            return a


def dual_direction(direction):
    if direction == 'N':
        return 'S'
    elif direction == 'S':
        return 'N'
    elif direction == 'W':
        return 'E'
    elif direction == 'E':
        return 'W'


def to_the_left(next_pos, next_pipe, entering_direction, exit_direction):
    if next_pipe == '|':
        if entering_direction == 'N':
            return {(next_pos[0], next_pos[1] + 1)}
        else:
            return {(next_pos[0], next_pos[1] - 1)}
    if next_pipe == '-':
        if entering_direction == 'W':
            return {(next_pos[0] - 1, next_pos[1])}
        else:
            return {(next_pos[0] + 1, next_pos[1])}
    if next_pipe == 'F':
        if entering_direction == 'S':
            return {(next_pos[0], next_pos[1] - 1), (next_pos[0] - 1, next_pos[1])}
        else:
            return {}
    if next_pipe == 'J':
        if entering_direction == 'N':
            return {(next_pos[0], next_pos[1] + 1), (next_pos[0] + 1, next_pos[1])}
        else:
            return {}
    if next_pipe == '7':
        if entering_direction == 'W':
            return {(next_pos[0] - 1, next_pos[1]), (next_pos[0], next_pos[1] + 1)}
        else:
            return {}
    if next_pipe == 'L':
        if entering_direction == 'E':
            return {(next_pos[0] + 1, next_pos[1]), (next_pos[0], next_pos[1] - 1)}
        else:
            return {}


right = {}
left = set()
path = {start_pos}
while True:
    next_pos = find_next_pos(current_pos, direction)
    next_pipe = maze[next_pos[0]][next_pos[1]]
    next_pipe_tuple = pipe_map[next_pipe]
    next_direction = find_next_direction(direction, next_pipe_tuple[0], next_pipe_tuple[1])

    if next_pos == start_pos:
        break
    else:
        current_pos = next_pos
        direction = next_direction
        path.add(next_pos)
        if path in left:
            left.remove(next_pos)
        
        candidates = to_the_left(next_pos, next_pipe, dual_direction(direction), next_direction)
        for cand in candidates:
            if cand not in path:
                left.add(cand)

for val in path:
    if val in left:
        left.remove(val)

# Remove the ones that are connected to the edge
def find_neighbours(a, b):
    return [(a + 1, b), (a - 1, b), (a, b + 1), (a, b - 1)]
    

actual = []
candidates = []
for (a, b) in left:
    candidates.append((a, b))
print(candidates)

while candidates:
    cand = candidates.pop()
    enclosed = True
    sequence = [cand]
    checked = []
    while sequence:
        current = sequence.pop()
        for neighbour in find_neighbours(current[0], current[1]):
            if neighbour in path or checked:
                pass
            elif neighbour[0] <= 0 or neighbour[0] >= num_rows - 1 or neighbour[1] <= 0 or neighbour[1] >= num_cols - 1:
                enclosed = False
                break
            else:
                sequence.append(neighbour)
        checked.append(current)
    if enclosed:
        actual += checked

print(set(actual))