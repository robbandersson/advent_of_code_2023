test_data = {7: 9,
             15: 40,
             30: 200}

data = {45: 305,
        97: 1062,
        72: 1110,
        95: 1695}

mult = 1
for time, record in data.items():
    num_ways = 0
    for speed in range(1, time):
        length = speed * (time - speed)
        if length > record:
            num_ways += 1
    mult *= num_ways

print(mult)

new_time = 45977295
new_record = 305106211101695

num_ways = 0
for speed in range(1, new_time):
    length = speed * (new_time - speed)
    if length > new_record:
        num_ways += 1

print(num_ways)
