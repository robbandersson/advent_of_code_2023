import os
import re
import sys
from functools import cache

sys.setrecursionlimit(1500)

data_dir = os.path.normpath("data")
with open(data_dir + '/12.txt') as f:
    data = f.read().strip().splitlines()


def num_hash_wanted(s):
    return list(map(int, re.findall(r'\b\d+\b', s)))


def num_hash_current(s):
    return s.count('#')


def remove_specific_num(s, num):
    return s.replace(str(num), '', 1)


def hash_sequence(s):
    if s[0] != '#':
        return 0

    hash_count = 1

    for char in s[1:]:
        if char == '#':
            hash_count += 1
        elif char == '.' or char == ' ':
            return hash_count
        else:
            return 0


@cache
def find_combinations(s):
    if s[0] == '.':
        return find_combinations(s[1:])

    if s[0] == ' ':
        return 0

    hs = hash_sequence(s)
    if hs > 0:
        if hs == num_hash_wanted(s)[0]:
            if len(num_hash_wanted(s)) == 1:
                return 1
            return find_combinations(remove_specific_num(s[hs:], hs))
        return 0

    if sum(num_hash_wanted(s)) == num_hash_current(s):
        return find_combinations(s.replace('?', '.', 1))
    return find_combinations(s.replace('?', '.', 1)) +\
        find_combinations(s.replace('?', '#', 1))


tot = 0
for row in data:
    #print(find_combinations(row), '\t', row)
    tot += find_combinations(row)

print(tot)

# Part 2


def replace_data(s):
    parts = s.split(' ', 1)    
    return (parts[0] + '?') * 4 + parts[0] + ' ' + (parts[1] + ',') * 4 +\
        parts[1]


tot = 0
for k, row in enumerate(data):
    new_row = replace_data(row)
    tot += find_combinations(new_row)
    print(f"{k} out {len(data)} calculated.")

print(tot)
