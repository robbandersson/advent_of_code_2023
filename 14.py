import os

data_dir = os.path.normpath("data")
with open(data_dir + '/14.txt') as f:
    data = f.read().strip().splitlines()


def tilt_north(pattern):
    while True:
        flag = False
        num_rows = len(pattern)
        for row in range(1, num_rows):
            for col in range(len(pattern[0])):
                if pattern[row][col] == 'O' and pattern[row-1][col] == '.':
                    pattern[row-1][col] = 'O'
                    pattern[row][col] = '.'
                    flag = True
        if not flag:
            break
    return pattern


def pattern_value(pattern):
    num_rows = len(pattern)
    num_cols = len(pattern[0])
    tot = 0
    for row in range(num_rows):
        for col in range(num_cols):
            if pattern[row][col] == 'O':
                tot += num_rows - row
    return tot


pattern = []
tot = 0
for row in data:
    if row == '':
        new_pattern = tilt_north(pattern)
        tot += pattern_value(new_pattern)
        pattern = []
    else:
        pattern.append(list(row))
new_pattern = tilt_north(pattern)
tot += pattern_value(new_pattern)

print(tot)

# Part 2


def tilt_south(pattern):
    while True:
        flag = False
        num_rows = len(pattern)
        for row in range(num_rows - 2, -1, -1):
            for col in range(len(pattern[0])):
                if pattern[row][col] == 'O' and pattern[row+1][col] == '.':
                    pattern[row+1][col] = 'O'
                    pattern[row][col] = '.'
                    flag = True
        if not flag:
            break
    return pattern


def tilt_east(pattern):
    while True:
        flag = False
        num_cols = len(pattern[0])
        num_rows = len(pattern)
        for col in range(num_cols - 2, -1, -1):
            for row in range(num_rows):
                if pattern[row][col] == 'O' and pattern[row][col+1] == '.':
                    pattern[row][col+1] = 'O'
                    pattern[row][col] = '.'
                    flag = True
        if not flag:
            break
    return pattern


def tilt_west(pattern):
    while True:
        flag = False
        num_cols = len(pattern[0])
        num_rows = len(pattern)
        for col in range(1, num_cols,):
            for row in range(num_rows):
                if pattern[row][col] == 'O' and pattern[row][col-1] == '.':
                    pattern[row][col-1] = 'O'
                    pattern[row][col] = '.'
                    flag = True
        if not flag:
            break
    return pattern


def cycle_pattern(pattern):
    return tilt_east(tilt_south(tilt_west(tilt_north(pattern))))


def print_pattern(pattern):
    for sublist in pattern:
        print(''.join(map(str, sublist)))


def generate_key(pattern):
    l = []
    num_rows = len(pattern)
    num_cols = len(pattern[0])
    for row in range(num_rows):
        for col in range(num_cols):
            if pattern[row][col] == 'O':
                l.append((row, col))
    return tuple(l)


pattern = []
tot = 0
for row in data:
    pattern.append(list(row))

seen_patterns = {}
first_key = generate_key(pattern)
seen_patterns[first_key] = 0
cycle = 0
while True:
    cycle += 1
    pattern = cycle_pattern(pattern)
    pattern_key = generate_key(pattern)
    if pattern_key in seen_patterns:
        cycle_length = cycle - seen_patterns[pattern_key]
        cycle_start = seen_patterns[pattern_key]
        print(cycle_start, cycle_length)
        break
    else:        
        seen_patterns[pattern_key] = cycle

remaining_cycles = (1000000000 - cycle_start) % cycle_length
for k in range(remaining_cycles):
    pattern = cycle_pattern(pattern)

print(pattern_value(pattern))