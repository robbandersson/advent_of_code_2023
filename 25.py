import os
import heapq
import random
import sys
sys.setrecursionlimit(60000)

data_dir = os.path.normpath("data")
with open(data_dir + '/25.txt') as f:
    data = f.read().strip().splitlines()


def add_path(graph, node1, node2):
    if node1 in graph:
        graph[node1].append(node2)
    else:
        graph[node1] = [node2]


# Create graph
graph = {}
for row in data:
    key, val = row.split(":")
    for node in val.split(" "):
        if node in (" ", ""):
            pass
        else:
            add_path(graph, key, node)
            add_path(graph, node, key)


def dijkstra(graph, start):
    distances = {vertex: float('infinity') for vertex in graph}
    predecessors = {vertex: None for vertex in graph}
    distances[start] = 0

    priority_queue = [(0, start)]

    while priority_queue:
        current_distance, current_vertex = heapq.heappop(priority_queue)

        # Check if the current path is no longer the shortest
        if current_distance > distances[current_vertex]:
            continue

        # Explore neighbors of the current vertex
        for neighbor in graph[current_vertex]:
            distance = current_distance + 1

            # Update the distance and predecessor if a shorter path is found
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                predecessors[neighbor] = current_vertex
                heapq.heappush(priority_queue, (distance, neighbor))

    return distances, predecessors


def reconstruct_path(predecessors, start, end):
    path = []
    current_vertex = end

    while current_vertex is not None:
        path.insert(0, current_vertex)
        current_vertex = predecessors[current_vertex]

    return path


def dfs(visited, graph, node):
    if node not in visited:
        visited.add(node)
        for neighbour in graph[node]:
            dfs(visited, graph, neighbour)


# Select a random start vertex

occurences = {}
for k, start_vertex in enumerate(list(graph.keys())):
    if (k + 1) % 100 == 0:
        break
    distances, predecessors = dijkstra(graph, start_vertex)
    for end_vertex in list(graph.keys()):
        if start_vertex == end_vertex:
            continue
        shortest_path = reconstruct_path(predecessors, start_vertex, end_vertex)
        for k in range(len(shortest_path) - 1):
            edge = (shortest_path[k], shortest_path[k + 1])
            reversed_edge = (shortest_path[k + 1], shortest_path[k])
            if edge in occurences:
                occurences[edge] += 1
            elif reversed_edge in occurences:
                occurences[reversed_edge] += 1
            else:
                occurences[edge] = 1

for val in heapq.nlargest(3, occurences.items(), key=lambda i: i[1]):
    edge = val[0]
    graph[edge[0]].remove(edge[1])
    graph[edge[1]].remove(edge[0])
    print(edge)


components = {}
candidates = set(graph.keys())
comp_val = 1
while True:
    start_node = random.choice(list(candidates))
    visited = set()
    dfs(visited, graph, start_node)
    for node in visited:
        components[node] = comp_val
    candidates = candidates.difference(set(components.keys()))

    if not candidates:
        break
    comp_val += 1

print("Number of components: ", len(set(components.values())))
prod = 1
for comp in set(components.values()):
    prod *= len([key for key in components.keys() if components[key] == comp])

print(prod)