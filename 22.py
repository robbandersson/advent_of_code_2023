import os
import heapq

data_dir = os.path.normpath("data")
with open(data_dir + '/22.txt') as f:
    data = f.read().strip().splitlines()


falling_queue = []
for brick in data:
    ((x1, y1, z1), (x2, y2, z2)) = tuple(tuple(map(int, group.split(','))) for
                                         group in brick.split('~'))
    falling_queue.append((min(z1, z2), ((x1, y1, z1), (x2, y2, z2))))
heapq.heapify(falling_queue)


def find_span(x1, x2, y1, y2):
    span = []
    if x1 == x2 and y1 != y2:
        for y in range(min(y1, y2), max(y1, y2) + 1):
            span.append((x1, y))
    elif y1 == y2 and x1 != x2:
        for x in range(min(x1, x2), max(x1, x2) + 1):
            span.append((x, y1))
    else:
        span.append((x1, y1))
    return span


def place_brick(grid_map, relations, id, x1, y1, z1, x2, y2, z2):
    span = find_span(x1, x2, y1, y2)
    z_max = 0
    connections = []
    for (x, y) in span:
        if (x, y) in grid_map:
            if grid_map[(x, y)]['height'] == z_max:
                connections.append(grid_map[(x, y)]['id'])
            elif grid_map[(x, y)]['height'] > z_max:
                z_max = grid_map[(x, y)]['height']
                connections = [grid_map[(x, y)]['id']]
    if len(span) == 1:
        grid_map[(x, y)] = {'height': z_max + abs(z1 - z2) + 1, 'id': id}
    else:
        for (x, y) in span:
            grid_map[(x, y)] = {'height': z_max + 1, 'id': id}
    connections = set(connections)
    if connections:
        relations[id] = {'parents': connections, 'children': None}
        for connected in connections:
            if relations[connected]['children'] is None:
                relations[connected]['children'] = [id]
            else:
                relations[connected]['children'].append(id)
    else:
        relations[id] = {'parents': None, 'children': None}
    return grid_map, relations


grid_map = {}
relations = {}
id = 0
while falling_queue:
    min_z, ((x1, y1, z1), (x2, y2, z2)) = heapq.heappop(falling_queue)
    grid_map, relations = place_brick(grid_map, relations, id, x1, y1, z1, x2,
                                      y2, z2)
    id += 1


tot = 0
for key, val in relations.items():
    if val['children'] is None:
        tot += 1
    else:
        for child in val['children']:
            if len(relations[child]['parents']) <= 1:
                break
        else:
            tot += 1

print(tot)

# Part 2

tot = 0
for key, val in relations.items():
    if val['children'] is None:
        continue
    falling = set()
    disintegrated = set()
    disintegrated.add(key)
    candidates = val['children']
    while candidates:
        child = candidates.pop(0)
        if len(set(relations[child]['parents']).difference(falling).difference(disintegrated)) < 1:
            falling.add(child)
            if relations[child]['children']:
                for grand_children in relations[child]['children']:
                    if grand_children not in falling and grand_children not in candidates:
                        candidates.append(grand_children)
    tot += len(falling)

print(tot)