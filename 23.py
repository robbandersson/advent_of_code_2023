# Read data
import os
import sys
sys.setrecursionlimit(60000)

data_dir = os.path.normpath("data")
with open(data_dir + '/23.txt') as f:
    data = f.read().strip().splitlines()

grid = []
for row in data:
    grid.append(list(row))


class Vertex:
    def __init__(self, label, position, category):
        self.label = label
        self.position = position
        self.category = category


class Edge:
    def __init__(self, start_vertex, end_vertex, weight):
        self.start_vertex = start_vertex
        self.end_vertex = end_vertex
        self.weight = weight


class Graph:
    def __init__(self):
        self.vertices = set()
        self.edges = set()

    def add_vertex(self, label, position, category):
        new_vertex = Vertex(label, position, category)
        self.vertices.add(new_vertex)

    def add_edge(self, start_vertex, end_vertex, weight=1):
        if start_vertex not in self.get_vertex_labels() or end_vertex not in self.get_vertex_labels():
            raise ValueError("Start or end vertex doesn't exist in graph!")
        new_edge = Edge(start_vertex, end_vertex, weight)
        self.edges.add(new_edge)

    def remove_vertex(self, label):
        self.vertices = {vertex for vertex in self.vertices if vertex.label != label}
        self.edges = {edge for edge in self.edges if label not in (edge.start_vertex, edge.end_vertex)}

    def remove_edge(self, edges):
        if isinstance(edges, list):
            self.edges = {edge for edge in self.edges if edge not in edges}
        else:
            self.edges = {edge for edge in self.edges if edge != edges}

    def get_vertex_label_by_position(self, position):
        for vertex in self.vertices:
            if vertex.position == position:
                return vertex.label
        return None

    def get_vertex_position_by_label(self, label):
        for vertex in self.vertices:
            if vertex.label == label:
                return vertex.position
        return None

    def get_vertex_positions(self):
        return [vertex.position for vertex in self.vertices]

    def get_vertex_labels(self):
        return [vertex.label for vertex in self.vertices]

    def get_edge_by_vertices(self, start_vertex, end_vertex):
        for edge in self.edges:
            if edge.start_vertex == start_vertex and edge.end_vertex == end_vertex:
                return edge
        return None

    def get_vertex(self, label):
        for vertex in self.vertices:
            if vertex.label == label:
                return vertex
        return None

    def get_edge_weight(self, start_vertex, end_vertex):
        for edge in self.edges:
            if edge.start_vertex == start_vertex and edge.end_vertex == end_vertex:
                return edge.weight
        return None

    def get_adjacent_vertices(self, vertex):
        adjacent = set()
        for edge in self.edges:
            if edge.start_vertex == vertex.label:
                adjacent.add(edge.end_vertex)
            elif edge.end_vertex == vertex.label:
                adjacent.add(edge.start_vertex)
        return adjacent

    def contraction(self):
        vertices_to_remove = [vertex for vertex in self.vertices if len(self.get_adjacent_vertices(vertex)) == 2]
        for vertex in vertices_to_remove:
            if vertex.category == '.':
                adjacent_vertices = list(self.get_adjacent_vertices(vertex))
                if len(adjacent_vertices) == 2:
                    new_weight = 0
                    for neighbour in adjacent_vertices:
                        new_weight += self.get_edge_weight(vertex.label, neighbour)
                    self.add_edge(adjacent_vertices[0], adjacent_vertices[1], new_weight)
                    self.add_edge(adjacent_vertices[1], adjacent_vertices[0], new_weight)
                    # Remove old vertex
                    self.remove_vertex(vertex.label)

    def get_edges(self):
        return [(edge.start_vertex, edge.end_vertex) for edge in self.edges]

    def longest_path_dfs(self, current, end, visited, current_path, current_length, max_path):
        visited.add(current)
        current_path.append(current)

        if current == end and current_length > max_path['length']:
            max_path['path'] = current_path.copy()
            max_path['length'] = current_length

        for neighbour in self.get_adjacent_vertices(self.get_vertex(current)):
            weight = graph.get_edge_weight(current, neighbour)
            if neighbour not in visited and weight:
                self.longest_path_dfs(neighbour, end, visited, current_path, current_length + weight, max_path)

        visited.remove(current)
        current_path.pop()

    def longest_path(self, start, end):
        visited = set()
        max_path = {'path': [], 'length': float('-inf')}
        self.longest_path_dfs(start, end, visited, [], 0, max_path)
        return max_path['path'], max_path['length']


def create_graph(grid):
    rows, cols = len(grid), len(grid[0])
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    direction_map = {'<': (0, -1), '>': (0, 1), '^': (-1, 0), 'v': (1, 0)}

    graph = Graph()

    # Add vertices
    label = 0
    for i in range(rows):
        for j in range(cols):
            if grid[i][j] == '#':
                continue
            category = grid[i][j]
            graph.add_vertex(label, (i, j), category)
            label += 1

    # Add edges
    for lbl in range(label):
        vertex = graph.get_vertex(lbl)
        position = graph.get_vertex_position_by_label(lbl)
        category = vertex.category
        if category == '.':
            steps = directions
        else:
            steps = [direction_map[category]]
        for step in steps:
            (i, j) = (position[0] + step[0], position[1] + step[1])
            if (i, j) in graph.get_vertex_positions():
                neighbour_lbl = graph.get_vertex_label_by_position((i, j))
                graph.add_edge(lbl, neighbour_lbl)

    # Remove edges that goes agains an arrow
    edges_to_remove = []
    for edge in graph.edges:
        vertex = graph.get_vertex(edge.end_vertex)
        category = vertex.category
        if category != '.':
            forbidden_direction = direction_map[category]
            end_pos = graph.get_vertex_position_by_label(edge.end_vertex)
            start_pos = graph.get_vertex_position_by_label(edge.start_vertex)
            if (start_pos[0] - end_pos[0], start_pos[1] - end_pos[1]) == forbidden_direction:
                edges_to_remove.append(edge)
    graph.remove_edge(edges_to_remove)

    return graph


graph = create_graph(grid)

longest_path, longest_path_length = graph.longest_path(0, len(graph.get_vertex_positions()) - 1)
print(longest_path_length)
