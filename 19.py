import os
import re

data_dir = os.path.normpath("data")
with open(data_dir + '/19.txt') as f:
    data = f.read().strip().splitlines()

workflows = {}
read_workflows = True
read_parts = False
tot = 0
for row in data:
    if read_parts:
        key = 'in'
        parts = row.replace('{', '').replace('}', '').split(',')
        parts = {item.split('=')[0]: item.split('=')[1] for item in parts}
        while True:
            for rule in workflows[key]:
                try:
                    logic, result = rule.split(':')
                    if eval(logic.replace(logic[0], parts[logic[0]])):
                        key = result
                        break
                except ValueError:
                    key = rule
                    break
            if key == 'A':
                tot += sum([int(val) for val in parts.values()])
                break
            if key == 'R':
                break

    if row == '':
        read_workflows = False
        read_parts = True

    if read_workflows:
        id, rules = row.replace('}', '').split('{')
        workflows[id] = rules.split(',')

print(tot)

# Part 2


def opposite(logic):
    if '<' in logic:
        return logic.replace('<', '>=')
    else:
        return logic.replace('>', '<=')


def find_path(workflows, key, pos, path, accepted_paths):
    rule = workflows[key][pos]
    try:
        logic, next_key = rule.split(':')
        alt_path = path.copy()
        alt_path.append(opposite(logic))
        path.append(logic)
        find_path(workflows, key, pos+1, alt_path, accepted_paths)

    except ValueError:
        next_key = rule

    if next_key == 'A':
        accepted_paths.append(path)
        return
    if next_key == 'R':
        return
    find_path(workflows, next_key, 0, path, accepted_paths)


accepted_paths = []
path = []
find_path(workflows, 'in', 0, path, accepted_paths)
tot = 0
for path in accepted_paths:
    limits = {}
    limits['x'] = {'upper': 4000, 'lower': 1}
    limits['m'] = {'upper': 4000, 'lower': 1}
    limits['a'] = {'upper': 4000, 'lower': 1}
    limits['s'] = {'upper': 4000, 'lower': 1}
    for constraint in path:
        num = int(re.search(r'\d+', constraint).group())
        key = constraint[0]
        if '>' in constraint and '>=' not in constraint and limits[key]['lower'] <= num:
            limits[key]['lower'] = num + 1
        elif '<' in constraint and '<=' not in constraint and limits[key]['upper'] >= num:
            limits[key]['upper'] = num - 1
        elif '>=' in constraint and limits[key]['lower'] < num:
            limits[key]['lower'] = num
        elif '<=' in constraint and limits[key]['upper'] > num:
            limits[key]['upper'] = num
    prod = 1
    for key, val in limits.items():
        prod *= (val['upper'] - val['lower'] + 1)
    tot += prod

print(tot)