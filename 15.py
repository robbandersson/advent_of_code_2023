import os
import re

data_dir = os.path.normpath("data")
with open(data_dir + '/15.txt') as f:
    data = f.read().strip().split(',')


tot = 0
for entry in data:
    curr_val = 0
    for char in entry:
        curr_val += ord(char)
        curr_val *= 17
        curr_val = curr_val % 256 
    tot += curr_val

print(tot)

# Part 2


def hashmap(s):
    val = 0
    for char in s:
        val += ord(char)
        val *= 17
        val = val % 256
    return val


box_map = {}
for entry in data:
    if '=' in entry:
        label = entry.split('=')[0]
        box = hashmap(label)
        s = entry.replace('=', ' ', 1)
        if box in box_map.keys():
            for k, val in enumerate(box_map[box]):
                if label in val:
                    box_map[box][k] = s
                    break
            else:
                box_map[box].append(s)
        else:
            box_map[box] = [s]
    if '-' in entry:
        label = entry.split('-')[0]
        box = hashmap(label)
        s = entry.replace('-', ' ', 1)
        if box in box_map.keys():
            for k, val in enumerate(box_map[box]):
                if label in val:
                    box_map[box].pop(k)
                    break

tot = 0
for box, l in box_map.items():
    for k, val in enumerate(l):
        tot += (box + 1) * (k + 1) * ([int(num) for num in re.findall(r'\d+', val)][0])

print(tot)