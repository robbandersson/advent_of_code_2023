import os
import math
data_dir = os.path.normpath("data")
with open(data_dir + '/09.txt') as f:
    lines = f.read().splitlines()
    data = [list(map(int, line.strip().split())) for line in lines]

tot = 0
for start in data:
    current = start
    last = [current[-1]]
    while not all(num == 0 for num in current):
        next_current = []
        for k in range(1, len(current)):
            next_current.append(current[k] - current[k - 1])

        current = next_current
        last.insert(0, current[-1])
    tot += sum(last)
print(tot)

tot = 0
for start in data:
    current = start
    first = [current[0]]
    while not all(num == 0 for num in current):
        next_current = []
        for k in range(1, len(current)):
            next_current.append(current[k] - current[k - 1])

        current = next_current
        first.insert(0, current[0])

    left_most = [0]
    for k in range(1, len(first)):
        left_most.append(first[k] - left_most[k - 1])
    # print(first)
    # print(left_most)
    # print(sum(left_most))
    tot += left_most[-1]

print(tot)