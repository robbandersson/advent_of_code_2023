#
# Solution to Advent of Code 2023 Day 4 - Scratchcards
#
# https://gitlab.com/robbandersson/advent_of_code_2023
#
import re
import os
data_dir = os.path.normpath("data")


def get_num(s):
    return re.findall(r'\d', s)


def compute():
    with open(data_dir + '/04.txt') as f:
        data = f.read().splitlines()
    part = 2

    tot = 0
    originals = {}
    copies = {}
    for line in data:
        card, line = line.split(':')
        card = int(re.findall(r'\d+', card)[0])
        nums1, nums2 = list(map(lambda s: re.findall(r'\d+', s), line.split('|')))
        matches = 0
        for num in nums2:
            if num in nums1:
                matches += 1
        if part == 1:
            if matches > 0:
                tot += 2 ** (matches - 1)
        else:
            originals[card] = 1
            if matches > 0:
                for j in range(1, matches + 1):
                    if card + j in copies:
                        if card in copies:
                            copies[card + j] += originals[card] + copies[card]
                        else:
                            copies[card + j] += originals[card]
                    else:
                        if card in copies:
                            copies[card + j] = originals[card] + copies[card]
                        else:
                            copies[card + j] = originals[card]

    tot = sum(originals.values()) + sum(copies.values())

    return tot


if __name__ == '__main__':
    print(compute())

