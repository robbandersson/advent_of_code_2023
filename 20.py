import os

data_dir = os.path.normpath("data")
with open(data_dir + '/20.txt') as f:
    data = f.read().strip().splitlines()


modules = {}
connected_input = {}
for entry in data:
    parts = entry.split()
    name = ''.join(char for char in parts[0] if char.isalpha())

    if '%' in entry:
        modules[name] = {'cat': 'flip_flop', 'status': 'off'}
    elif '&' in entry:
        modules[name] = {'cat': 'conjunction', 'status': {}}
    else:
        modules[name] = {'cat': 'other'}
    modules[name]['destinations'] = [s.replace(',', '') for s in parts[2:]]
    for dest_module in modules[name]['destinations']:
        if dest_module in connected_input:
            connected_input[dest_module].append(name)
        else:
            connected_input[dest_module] = [name]

for key, vals in modules.items():
    if vals['cat'] == 'conjunction':
        vals['status'] = {connected: 'low' for connected in connected_input[key]}


def push_button(modules, number_of_pushs):
    tot = 0
    num_low = 0
    num_high = 0
    for k in range(number_of_pushs):
        queue = [('button', 'broadcaster', 'low')]
        while queue:
            sender, receiver, signal = queue.pop(0)
            if signal == 'low':
                num_low += 1
            else:
                num_high += 1
            next_sender = receiver
            if receiver not in modules:
                continue
            else:
                if modules[receiver]['cat'] == 'flip_flop':
                    if signal == 'low':
                        if modules[receiver]['status'] == 'off':
                            modules[receiver]['status'] = 'on'
                            next_signal = 'high'
                        else:
                            modules[receiver]['status'] = 'off'
                            next_signal = 'low'
                        for next_receiver in modules[receiver]['destinations']:
                            queue.append((next_sender, next_receiver, next_signal))

                elif modules[receiver]['cat'] == 'conjunction':
                    modules[receiver]['status'][sender] = signal
                    for connected in modules[receiver]['status'].values():
                        if connected == 'low':
                            next_signal = 'high'
                            break
                    else:
                        next_signal = 'low'
                    for next_receiver in modules[receiver]['destinations']:
                        queue.append((next_sender, next_receiver, next_signal))

                elif receiver == 'broadcaster':
                    for next_receiver in modules[receiver]['destinations']:
                        queue.append((next_sender, next_receiver, signal))
    tot = num_high * num_low
    return tot


print(push_button(modules, 1000))